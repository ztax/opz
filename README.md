# `opz`
### Status: early WIP

### Goal
manage git repos for deployed docker-compose services
- easily init the repo
  - auto-detect & .gitignore volume directories
  - verifying to unwanted files are commited (dry-run)
  - with a canonical default repo location, e.g. `gitlab.com:team/ops.git`
- (TODO) show diff & commit updates
- (TODO) handle snapshots, backups, restore
- ...
