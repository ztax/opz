use std::{ffi::OsStr, path::PathBuf, process::Command};

pub struct Service {
    pub name: String,
    pub dir: PathBuf,
}

impl Service {
    pub fn cmd<I, S>(&self, program: &str, args: I) -> Command
    where
        I: IntoIterator<Item = S>,
        S: AsRef<OsStr>,
    {
        let mut command = Command::new(program);
        command.current_dir(&self.dir).args(args);
        command
    }
}
