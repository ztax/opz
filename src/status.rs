use std::process::Command;

use anyhow::{Context, Result};

use crate::{
    git::{git_has_remote, git_init, git_is_repo},
    gitlab::{git_push_withtoken, gitlab_create, GitlabRepo},
    service::Service,
};

pub fn svc_status_info(svc: &Service, repo: &GitlabRepo) -> Result<()> {
    println!("Repo: {}", repo.web_url);
    Ok(())
}
