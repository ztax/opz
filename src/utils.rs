use anyhow::{anyhow, Context, Error, Result};
use inline_colorization::*;
use std::{
    io::{stdin, stdout, Write},
    process::Command,
};

pub fn assert_cmd(cmd: &mut Command) -> Result<()> {
    // println!("$ {:?}", cmd); # - includes `cd cwd/ &&`
    println!(
        "{color_blue}$ {} {}{color_reset}",
        cmd.get_program().to_string_lossy(),
        cmd.get_args()
            .map(|arg| {
                let stringified = arg.to_string_lossy().into_owned();
                if stringified.contains(' ') {
                    format!("'{}'", stringified)
                } else {
                    stringified
                }
            })
            .collect::<Vec<String>>()
            .join(" ")
    );
    let status = cmd
        .status()
        .with_context(|| format!("Command: {:?}", cmd))?;
    println!();
    if !status.success() {
        Err(anyhow!(
            "exit code: {}",
            status.code().map_or("?".to_owned(), |c| c.to_string())
        ))
    } else {
        Ok(())
    }
}

pub fn prompt_yes_no(question: &str) -> Result<bool> {
    let mut input = String::new();
    loop {
        print!("{} (y/n): ", question);
        stdout().flush().context("flush stdout")?; // Ensure the question is printed before reading input

        input.clear(); // Clear previous input
        stdin().read_line(&mut input).context("read prompt")?;

        match input.trim().to_lowercase().as_str() {
            "y" | "yes" => return Ok(true),
            "n" | "no" => return Ok(false),
            _ => println!("Please enter 'y' or 'n'."),
        }
    }
}
