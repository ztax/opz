use std::process::Command;

use anyhow::{Context, Result};

use crate::{
    git::{git_has_remote, git_is_repo},
    gitlab::{git_push_withtoken, GitlabRepo},
    service::Service,
    status::svc_status_info,
};

pub fn svc_push(svc: &Service) -> Result<()> {
    git_is_repo(svc).context("check if git repo")?;
    let http_url_to_repo = git_has_remote(&svc)
        .context("check git remote")?
        .context("ensure remote")?;
    let repo = GitlabRepo::infer(&http_url_to_repo)?;
    //git_remote_ensure(svc, &repo.http_url_to_repo).context("git remote add")?
    git_push_withtoken(svc, &http_url_to_repo).context("git init push")?;

    svc_status_info(svc, &repo)?;
    Ok(())
}
