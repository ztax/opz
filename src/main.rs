#![allow(unused_imports)] // they annoy during development

use anyhow::{bail, Context, Result};
use clap::{Parser, Subcommand};
use init::svc_init;
use push::svc_push;
use service::Service;
use std::env;
use std::path::PathBuf;

mod git;
mod gitlab;
mod init;
mod push;
mod service;
mod status;
mod utils;

// clap docs: https://docs.rs/clap/latest/clap/_derive/index.html
/// OPS helper
#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Option<Commands>,

    /// Set dir instead of PWD
    #[arg(short, long, value_name = "DIR")]
    dir: Option<PathBuf>,

    /// Turn debugging information on (TODO: doesn't do anything)
    #[arg(short, long, action = clap::ArgAction::Count)]
    verbose: u8,
}

#[derive(Subcommand)]
enum Commands {
    /// Initialize a svc
    Init {},
    Status {},
    Push {},
}

fn main() -> Result<()> {
    let cli = Cli::parse();
    let dir_specified = cli.dir.is_some();
    let svc_dir = cli.dir.unwrap_or(env::current_dir().context("get cwd")?);

    let parent_dir = svc_dir.parent().context("get parent directory")?;
    if !dir_specified && parent_dir.file_name().context("parent dir name")?.to_str() != Some("srv")
    {
        bail!("Not in a subdirectory of 'srv'"); //HACK: proper define this via global config/env
    }
    let svc = Service {
        dir: svc_dir.clone().to_owned(),
        name: svc_dir
            .file_name()
            .context("get current directory name")?
            .to_str()
            .context("convert dirname to str")?
            .to_owned(),
    };

    match &cli.command {
        Some(Commands::Init {}) => {
            svc_init(&svc)?;
        }
        Some(Commands::Push {}) => {
            svc_push(&svc)?;
        }
        Some(Commands::Status {}) | None => {
            todo!("status {:?}", svc_dir);
        }
    };
    Ok(())
}
