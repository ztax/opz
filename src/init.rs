use std::process::Command;

use anyhow::{Context, Result};

use crate::{
    git::{git_has_remote, git_init},
    gitlab::{git_push_withtoken, gitlab_create, GitlabRepo},
    service::Service,
    status::svc_status_info,
};

pub fn svc_init(svc: &Service) -> Result<()> {
    git_init(svc).context("git init")?;
    let repo = match git_has_remote(&svc)? {
        Some(http_url_to_repo) => GitlabRepo::infer(&http_url_to_repo)?,
        None => {
            let repo = gitlab_create(&svc.name).context("create gitlab project")?;
            git_push_withtoken(svc, &repo.http_url_to_repo).context("git init push")?;
            repo
        }
    };

    svc_status_info(svc, &repo)?;
    Ok(())
}
