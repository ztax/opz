use crate::service::Service;
use crate::utils::assert_cmd;
use anyhow::{bail, Context, Result};
use inline_colorization::*;
use rpassword::read_password;
use serde::Deserialize;
use std::env;
use std::io::Write;
use std::process::Command;
use std::str::from_utf8;
use ureq;
use ureq::json;
use ureq::serde_json::Value;

#[derive(Deserialize)]
pub struct GitlabRepo {
    pub web_url: String,
    pub http_url_to_repo: String,
}

pub fn gitlab_create(svc_name: &str) -> Result<GitlabRepo> {
    println!(
        "{style_bold}➤ Creating gitlab project '{}'{style_reset}",
        svc_name
    );

    let namespace_id = env::var("OPZ_GITLAB_GROUP")
        .context("get group ID from OPZ_GITLAB_GROUP")?
        .parse::<u32>()
        .context("parse group ID from OPZ_GITLAB_GROUP")?;
    let token = get_manager_token()?;

    let response = ureq::post("https://gitlab.tamera.org/api/v4/projects")
        .set("Private-Token", &token)
        .send_json(&json!({ "name": svc_name, "namespace_id": namespace_id }));
    // dbg!(&response);

    let repo_info: GitlabRepo = match response {
        Err(ureq::Error::Status(code, response)) => {
            bail!("Gitlab API {}: {}", code, response.into_json::<Value>()?)
        }
        Err(err) => {
            bail!("Error calling GitLab: {}", err)
        }
        Ok(response) => response.into_json().context("parse gitlab response")?,
    };

    println!();
    Ok(repo_info)
}

pub fn git_push_withtoken(svc: &Service, url: &str) -> Result<()> {
    println!(
        "{style_bold}➤ Pushing to gitlab project '{}'{style_reset}",
        url
    );
    // (i) not using assert_cmd as we don't wanna print the cmd (bc. of the token)
    let result = svc
        .cmd(
            "git",
            [
                "push",
                &url.replace(
                    "https://",
                    &format!("https://opz:{}@", get_manager_token()?),
                ),
                "main",
            ],
        )
        .status()
        .context("Failed initial git push")?;
    if !result.success() {
        bail!("Failed initial git push: {:?}", result.code());
    }
    println!();
    Ok(())
}

impl GitlabRepo {
    pub fn infer(http_url_to_repo: &str) -> Result<GitlabRepo> {
        Ok(GitlabRepo {
            http_url_to_repo: http_url_to_repo.to_owned(),
            web_url: http_url_to_repo
                .strip_suffix(".git")
                .with_context(|| format!("strip '.git' from remote url: {}", http_url_to_repo))?
                .to_owned(),
        })
    }
}

pub fn get_manager_token() -> Result<String> {
    Ok(env::var("OPZ_GITLAB_MANAGER_TOKEN").or_else(|_| {
        if let Ok(pass_path) = env::var("OPZ_GITLAB_MANAGER_PASS_PATH") {
            let output = Command::new("pass")
                .args(["-o", &pass_path])
                .output()
                .context("get pass")?;
            if !output.status.success() {
                bail!("failed to get pass {}", pass_path)
            }
            return Ok(from_utf8(&output.stdout)?.trim().to_owned());
        }
        println!("env missing: OPZ_GITLAB_MANAGER_TOKEN");
        println!("Hint: read -sx OPZ_GITLAB_MANAGER_TOKEN");
        print!("Enter GitLab Token to create project: ");
        std::io::stdout().flush().expect("flush stdout");
        let token = read_password().expect("read password");
        Ok(token.trim().to_owned())
    })?)
}
