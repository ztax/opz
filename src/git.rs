use crate::{
    service::Service,
    utils::{assert_cmd, prompt_yes_no},
};
use anyhow::{anyhow, bail, Context, Result};
use git2::{Repository, RepositoryInitOptions};
use inline_colorization::*;
use std::process::{exit, Command};

pub fn git_init(svc: &Service) -> Result<()> {
    println!(
        "{style_bold}➤ Checking repo {}{style_reset}",
        &svc.dir.display()
    );
    let repo = match Repository::open(&svc.dir) {
        Ok(repo) => repo,
        Err(_) => {
            println!("Initializing Git Repo");
            let mut opts = RepositoryInitOptions::new();
            opts.initial_head("main"); // server has git version that still uses hardcoded 'master'
            Repository::init_opts(&svc.dir, &opts).context("init git repo")?
        }
    };

    println!("Ensuring initial commit");
    // dbg!(repo.revparse_single("HEAD"));
    if repo.revparse_single("HEAD").is_err() {
        assert_cmd(&mut svc.cmd("git", ["commit", "--allow-empty", "-m", "empty init"])) // needed so that the next push can work with scoped access token
            .context("Failed add empty commit")?;
    }

    println!("Ensuring something is committed");
    if is_head_initial_commit(&repo).context("check if HEAD contains files")? {
        println!("With current settings, git would add:\n");
        assert_cmd(&mut svc.cmd("git", ["add", "-n", "."]))
            .context("Failed to simulate git add")?;

        match prompt_yes_no("Does this look sound?")? {
            false => {
                println!("edit .gitignore & run this command again");
                exit(0);
            }
            true => {
                assert_cmd(&mut svc.cmd("git", ["add", "."]))
                    .context("Failed to simulate git add")?;
            }
        }

        // repo.commit(
        //     Some("HEAD"),
        //     &repo.signature()?,
        //     &repo.signature()?,
        //     "Init",
        //     &repo.tree(repo.revparse_single("HEAD")?.id())?,
        //     &[],
        // )
        // .context("git commit")?;
        assert_cmd(Command::new("git").current_dir(&svc.dir).args([
            "commit",
            "-m",
            "Initial service files",
        ]))
        .context("Failed to create initial commit")?;
    }
    println!();
    Ok(())
}
pub fn git_is_repo(svc: &Service) -> Result<()> {
    println!("Checking repo {}", &svc.dir.display());
    let _repo = Repository::open(&svc.dir).context("open repo")?;
    //TODO: more sanity checks
    Ok(())
}
// pub fn git_push(svc: &Service) -> Result<()> {
//     println!("Pushing repo {}", &svc.dir.display());
//     assert_cmd(&mut svc.cmd("git", ["push", "origin", "main", "--set-upstream"]))
//         .context("Failed to git push")?;
//     Ok(())
// }
pub fn git_has_remote(svc: &Service) -> Result<Option<String>> {
    let repo = Repository::open(&svc.dir).context("open repo")?;
    let url = match repo.find_remote("origin") {
        Ok(remote) => Ok(Some(
            remote
                .url()
                .context("remote 'origin' exists but has no url")?
                .to_owned(),
        )),
        Err(_) => Ok(None),
    };
    url
}
// pub fn git_remote_ensure(svc: &Service, url: &str) -> Result<()> {
//     let repo = Repository::open(&svc.dir).context("open repo")?;

//     match repo.find_remote("origin") {
//         Ok(remote) => {
//             if !remote.url().is_some_and(|u| u == url) {
//                 bail!("Remote 'origin' has wrong url: {:?}", remote.url());
//                 // match prompt_yes_no("Overwrite (if not we will abort)?")? {
//                 //     false => {
//                 //         println!("edit .gitignore & run this command again");
//                 //         exit(0);
//                 //     }
//                 //     true => {
//                 //         assert_cmd(
//                 //             &mut svc.cmd("git", ["add", "."]),
//                 //             anyhow!("Failed to simulate git add"),
//                 //         )?;
//                 //     }
//                 // }
//             }
//         }
//         Err(_) => {
//             repo.remote("origin", url)?;
//             // assert_cmd(
//             //     &mut svc.cmd("git", ["remote", "add", "origin", url]),
//             //     anyhow!("Failed to add remote"),
//             // )?;
//         }
//     }
//     Ok(())
// }

// pub fn git_status(svc: &Service) -> Result<()> {
//     let repo = Repository::open(&svc.dir).context("open repo")?;
//     let mut index = repo.index()?;
//     let diff = repo.diff_tree_to_index(Some(&repo.head()?.peel_to_tree()?), Some(&index), None)?;

//     dbg!(diff.deltas().len());
//     let statuses = &repo.statuses(None).context("git statuses")?;
//     dbg!(statuses
//         .iter()
//         .map(|s| format!("{:?} {:?}", s.status(), s.path()))
//         .collect::<Vec<String>>());

//     todo!("status stuff")
// }

fn is_head_initial_commit(repo: &Repository) -> Result<bool> {
    let head = repo.head()?.peel_to_commit()?;
    let tree = head.tree()?;

    // Check if the tree is empty or not
    Ok(tree.is_empty())
}
